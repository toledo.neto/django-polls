# Django Polls Project

Replicação do projeto Django de introdução da [documentação oficial](https://docs.djangoproject.com/pt-br/2.2/intro/tutorial01/).
Necessário:
* DB PostgreSQL

### Envars necessárias
SECRET_KEY: Django secret key
DEBUG: Debug mode
ALLOWED_HOSTS: Allowed Hosts
NAME_PSQL: DB name
USER_PSQL: DB User
PASSWORD_PSQL: DB password
HOST_PSQL: DB Host
PORT_PSQL(default = 5432): DB Port
